const requestURLUsers = `https://ajax.test-danit.com/api/json/users`;
const requestURLPosts = `https://ajax.test-danit.com/api/json/posts`;
const usersRequest = fetch(requestURLUsers).then((response) => response.json());
const postsRequest = fetch(requestURLPosts).then((response) => response.json());
Promise.all([usersRequest, postsRequest]).then((value) => {
  let [users, posts] = value;
  showPost(users, posts);
});

const blockCards = document.querySelector(".cards");
blockCards.addEventListener("click", function (event) {
  const remove = new CardDisplay(event);
  remove.removeFromHtml(event);
});

function showPost(users, posts) {
  users.forEach(function (item) {
    let user = new User(item);
    posts.forEach(function (itemPost) {
      let post = new Post(itemPost);
      if (user.id === post.userId) {
        const printer = new CardDisplay(new Card({ ...user, ...post }));
        printer.addToHtml();
      }
    });
  });
}

class User {
  constructor(item) {
    this.name = item.name;
    this.email = item.email;
    this.id = item.id;
  }
}
class Post {
  constructor(item) {
    this.userId = item.userId;
    this.postId = item.id;
    this.title = item.title;
    this.body = item.body;
  }
}
class Card extends Post {
  constructor(options) {
    super(options);
    this.postId = options.postId;
    this.name = options.name;
    this.email = options.email;
  }
}
class CardDisplay {
  constructor(card) {
    this.card = card;
  }
  addToHtml() {
    let stringPost = `<div class='js-card'>
        <button class="js-delete" data-post-id="${this.card.postId}">X</button>
        <h4 class='js-title'>${this.card.title}</h4>
        <p>${this.card.body}<p/>
        <p>${this.card.name}
        <a class='js-mail' href="mailto:${this.card.email}?" target="_blank">${this.card.email}</a>
        </p>
        </div>`;
        blockCards.insertAdjacentHTML("beforeend", `${stringPost}`);
  }
  removeFromHtml(event) {
    if (event.target.classList.contains("js-delete")) {
      const idPost = event.target.getAttribute("data-post-id");
      fetch(`https://ajax.test-danit.com/api/json/posts/${idPost}`, {
        method: "DELETE",
      })
        .then((response) => {
          if (response.ok) {
            event.target.parentNode.remove();
            console.log("Deleted post number " + idPost);
            console.log(response);
            return response;
          }
        })
        .catch(function (e) {
          console.log(e);
        });
    }
  }
}
